package com.devtold.web.service;

import com.devtold.web.entites.securityEntiry.AppRole;

import java.util.List;

/**
 * Methode utiliser pour le traitement des roles
 */
public interface AppRoleService {

    //ajouter un role
    AppRole addNewRole(AppRole role);
    List<AppRole> listRole();
    AppRole findByRoleName(String name);


}
