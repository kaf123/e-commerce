package com.devtold.web.service;

import com.devtold.web.entites.Categories;

import java.util.List;
import java.util.Optional;

public interface CategorieService {
    List<Categories> listCategorie();
    Categories ajouterCategorie (Categories categories);
    Categories modifierCategories(Categories categories);
    Categories supprimercategories(Long id);
    Optional<Categories> findById(Long id);

}
