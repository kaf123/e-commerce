package com.devtold.web.service;

import com.devtold.web.entites.Produits;

import java.util.List;
import java.util.Optional;

public interface ProduitsService {

    Produits ajouterProduits(Produits produits);
    void SupprimerProduits(Long ids);
    Produits modifierProduits(Produits produits);
    List<Produits> listTout();
    List<Produits> listerParMotCle(String motCle);
    List<Produits> listerParCategories(Long ids);
    List<Produits> listerProduitsSelectionner();
    Optional<Produits> findById(Long ids);
    Produits InitialisationProduits(Produits produits);
    Produits findByNomProduit(String  nomP);
    Produits findByPrix(double nbre);
    Produits findByQuantite(double nbre);
    Produits findByDescription(String  nomP);





}
