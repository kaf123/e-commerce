package com.devtold.web.service;

import com.devtold.web.entites.securityEntiry.AppUser;

import java.util.List;

/**
 * Methode à utiliser pour le traitement
 */

public interface AppUserService {
    //ajouter un utilisateur
    AppUser addNewUser(AppUser user);
    //rechercher un utilisateur par son nom
    AppUser loadUserByName(String  name);
    //afficher la liste des utilisateurs
    List<AppUser> findAllUser();
    //ajouter un role
    void addRoleToUser(String roleName, String userName);
    AppUser findByUserName(String name);




}
