package com.devtold.web.controller;

import com.devtold.web.entites.securityEntiry.AddRoleToUserDto;
import com.devtold.web.entites.securityEntiry.AppRole;
import com.devtold.web.entites.securityEntiry.AppUser;
import com.devtold.web.serviceImpl.AppRoleServiceImpl;
import com.devtold.web.serviceImpl.AppUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("appuser")
public class AppUserController {
    @Autowired
    private AppUserServiceImpl appUserService;

    @Autowired
    private AppRoleServiceImpl appRoleService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/list")
    public List<AppUser> userList(){

        return appUserService.findAllUser();
    }

    @PostMapping("/add")
    public AppUser CrtAddUser(@RequestBody AppUser user){
        if (!(user instanceof AppUser)){
            throw new RuntimeException();
        }
        if (user.getUserName().isEmpty() || user.getPassWord().isEmpty()){
            throw  new RuntimeException();
        }
        //encodage du mot de passe
        String pw =user.getPassWord();
        AppUser createUser = new AppUser();
        createUser.setUserName(user.getUserName());
        createUser.setPassWord(passwordEncoder.encode(pw));

        return appUserService.addNewUser(createUser);
    }

    @PostMapping("addRoleToUser")
    public  void addRooleToUser(@RequestBody AddRoleToUserDto dto){
        AppUser selectUser = appUserService.findByUserName(dto.getUsername());
        AppRole selectRole = appRoleService.findByRoleName(dto.getRolename());
        selectUser.getAppRoles().add(selectRole);
        

    }

}
