package com.devtold.web.controller;

import com.devtold.web.Repository.CategorieRepository;
import com.devtold.web.entites.Categories;
import com.devtold.web.entites.Produits;
import com.devtold.web.serviceImpl.ProduitsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/produits")
public class ProduitController {

    @Autowired
    private ProduitsServiceImpl produitsService;

    @Autowired
    private CategorieRepository categorieRepository;

  @PostMapping(path = "/add")
    ResponseEntity<?> ajouterProduits(@ModelAttribute Produits produits){
        Produits pro = new Produits();

       if (!(pro == null)){
               pro.setNomProduit(produits.getNomProduit());
               pro.setDescription(produits.getDescription());
               pro.setPhoto(produits.getFile().getOriginalFilename());
               pro.setPrix(produits.getPrix());
               pro.setQuantite(produits.getQuantite());
               pro.setCategories(produits.getCategories());
               pro.setSelectionne(produits.isSelectionne());
          }else
          {
           throw new RuntimeException(" veuillez renseigné les champs obligatoire");
       }
       pro = produitsService.ajouterProduits(produits);
        return  new ResponseEntity<>(pro,HttpStatus.CREATED);
    }

    @GetMapping("/listall")
    public ResponseEntity<?> listerToutProduits( ){
      List<Produits> pt = produitsService.listTout();
        if (pt == null || pt.size()<=0)throw new RuntimeException("liste vide");
        return new ResponseEntity<>(pt,HttpStatus.OK);
    }

    @GetMapping("/listname")
        public ResponseEntity<?> listerParNom (String noms){
      List<Produits> pt = produitsService.listerParMotCle(noms);
      return new ResponseEntity<>(pt,HttpStatus.OK);
    }

    @GetMapping("/listid")
    public ResponseEntity<?> listerParid(Long ids){
      Optional<Produits> pts = produitsService.findById(ids);
      return new ResponseEntity<>(pts,HttpStatus.OK);
    }
    
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> supprimerProduits( Long ids){

         produitsService.SupprimerProduits(ids);
     return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public  ResponseEntity<?> modifierProduit(@PathVariable Long id,@RequestBody Produits produits ){
        Optional<Produits> pro = produitsService.findById(id);
        if (pro.isEmpty()){
            throw new RuntimeException("l'identifiant n'existe pas");
        }
      Produits modProduits =(Produits)pro.get();
      modProduits.setNomProduit(produits.getNomProduit());
      modProduits.setCategories(produits.getCategories());
      modProduits.setPrix(produits.getPrix());
      modProduits.setSelectionne(produits.isSelectionne());
      modProduits.setDescription(produits.getDescription());
      //creer un route pour passer les images
     //modProduits.setPhoto(produits.getFile().getOriginalFilename());

       Produits p = produitsService.modifierProduits(modProduits);
      return new ResponseEntity<>(p,HttpStatus.OK);
    }


    @PostMapping("/initProduits")
    public ResponseEntity<?> InitilisalysedProducts(HttpServletRequest request){
      List<Produits> produitsList= new ArrayList<>();
      Produits.produitsInit[] produitsInits= Produits.produitsInit.values();

      for (Produits.produitsInit init: produitsInits){

          String categorieinit=init.getCategoriesInit().getLebelle();
          Optional<Categories> categories= Optional.ofNullable(categorieRepository.findByNomCategorie(categorieinit));

          /*  String libelleInit =init.getLibelle();
          Optional<Produits> produits= Optional.ofNullable(produitsService.findByNomProduit(libelleInit));
          String description=init.getDescription();
          Optional<Produits> prodDescrip = Optional.ofNullable(produitsService.findByDescription(description));
          double quatite =init.getQuantites();
          Optional<Produits> proQuant= Optional.ofNullable(produitsService.findByPrix(quatite));
          double prix=init.getPrix();
          Optional<Produits> proPrix= Optional.ofNullable(produitsService.findByQuantite(prix));*/

          produitsList.add(new Produits(init.getLibelle(),init.getDescription(),init.getQuantites(),categories.get()));

      }
      produitsService.InitialisationProduits((Produits) produitsList);
      return new ResponseEntity<>(HttpStatus.OK);
    }

}
