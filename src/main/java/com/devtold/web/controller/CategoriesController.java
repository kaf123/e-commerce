package com.devtold.web.controller;

import com.devtold.web.entites.Categories;
import com.devtold.web.serviceImpl.CategorieServiceimpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/categories")
public class CategoriesController {

    @Autowired
    private CategorieServiceimpl serviceimpl;

    @GetMapping(path = "/list")
    public ResponseEntity<?> listCategorie(){
        List<Categories> CT = serviceimpl.listCategorie();
        return new ResponseEntity<>(CT,HttpStatus.OK);
    }

    @PostMapping(path = "/add")
    public ResponseEntity<?> addCategories(@ModelAttribute Categories categories){

      Categories CT = new Categories();

      if (!(CT == null)){
          CT.setNomCategorie(categories.getNomCategorie());
          CT.setDescription(categories.getDescription());
          CT.setPhoto(categories.getFile().getOriginalFilename());
      }else {
          throw new RuntimeException("Veuillez renseigné champ obligatoire");
      }
          serviceimpl.ajouterCategorie(CT) ;
          return new ResponseEntity<>(CT, HttpStatus.CREATED);
    }


    @PutMapping("/modifier/{id}")
    public ResponseEntity<?> updateCategorie(@PathVariable Long id,@RequestBody Categories categories) {
        Optional<Categories> ct = serviceimpl.findById(id);
        if (ct == null)throw new RuntimeException("  ");

        Categories cat = serviceimpl.modifierCategories(ct.get());
        cat.setNomCategorie(categories.getNomCategorie());
        cat.setDescription(categories.getDescription());
        cat.setPhoto(categories.getPhoto());
        serviceimpl.modifierCategories(cat);

        return new ResponseEntity<>(cat,HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public  ResponseEntity<?>  deleteCategories(@PathVariable Long id){
       Categories categories = serviceimpl.supprimercategories(id);
        return new ResponseEntity<>(categories,HttpStatus.OK);
    }

}
