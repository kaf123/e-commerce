package com.devtold.web.controller;

import com.devtold.web.entites.securityEntiry.AppRole;
import com.devtold.web.serviceImpl.AppRoleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("approle")
public class AppRoleController {
    @Autowired
    private AppRoleServiceImpl appRoleService;

    @GetMapping("/list")
    public List<AppRole> listAllRoles( ){

        return appRoleService.listRole();
    }

    @PostMapping("/add")
    public AppRole createrole(@RequestBody  AppRole roleDto){
        if (!(roleDto instanceof AppRole)){
            throw  new RuntimeException();
        }
        if (roleDto.getRoleName().isEmpty() || roleDto.getRoleName()==null){
            throw new RuntimeException();
        }
        AppRole createR = new AppRole();
        createR.setRoleName(roleDto.getRoleName());

        return appRoleService.addNewRole(createR);
    }


}

