package com.devtold.web.Repository;

import com.devtold.web.entites.securityEntiry.AppRole;
import com.devtold.web.entites.securityEntiry.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppRoleRepository extends JpaRepository<AppRole,Long> {

    AppRole findByRoleName(String name);
}
