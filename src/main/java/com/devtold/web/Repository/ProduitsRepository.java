package com.devtold.web.Repository;

import com.devtold.web.entites.Produits;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProduitsRepository extends JpaRepository<Produits,Long> {

    Produits findByNomProduit(String  nomP);
    Optional<Produits> findById(Long ids);
    Produits findByPrix(double nbre);
    Produits findByQuantite(double nbre);
    Produits findByDescription(String  nomP);

    //lister tout par ordre alphabetique
    List<Produits> findAllByOrderByNomProduitAsc();
    //lister avec plusieur mot cle
//    List<Passenger> findByLastNameOrderBySeatNumberAsc(String lastName);
    //Requette permettant de lister tout les produits par ordre alphabetique selon le prix et la quantite
    List<Produits> findAllByPrixAndQuantiteOrderByNomProduitAsc(double prix, double quantite);

//    List<Produits> findByOrderBySeatNumberAsc();
//findAllByOrderByIdAsc();






}
