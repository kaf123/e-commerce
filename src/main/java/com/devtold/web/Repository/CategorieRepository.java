package com.devtold.web.Repository;

import com.devtold.web.entites.Categories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategorieRepository extends JpaRepository<Categories,Long> {

    Categories findByNomCategorie(String nom);

    Optional<Categories>  findById(Long id);

    Optional<Categories> findByNomCategorieAndDescription(String nom, String description);

}
