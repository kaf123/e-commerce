package com.devtold.web.serviceImpl;

import com.devtold.web.Repository.ProduitsRepository;
import com.devtold.web.entites.Produits;
import com.devtold.web.service.ProduitsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProduitsServiceImpl implements ProduitsService {




    @Autowired
    private ProduitsRepository produitsR;

    @Override
    public Produits ajouterProduits(Produits produits) {
        return produitsR.save(produits);
    }

    @Override
    public void SupprimerProduits(Long ids) {
        produitsR.deleteById(ids);
    }

    @Override
    public Produits modifierProduits(Produits produits) {

        return produitsR.save(produits);
    }

    @Override
    public List<Produits> listTout() {
        return produitsR.findAll();
    }

    @Override
    public List<Produits> listerParMotCle(String motCle) {

        return (List<Produits>) produitsR.findByNomProduit(motCle);
    }

    @Override
    public List<Produits> listerParCategories(Long ids) {
        return null;
    }

    @Override
    public List<Produits> listerProduitsSelectionner() {
        return null;
    }

    @Override
    public Optional<Produits> findById(Long ids) {
        Optional<Produits> pt = produitsR.findById(ids);
        if (pt == null)throw new RuntimeException("id inexistante");
        return pt;
    }

    public Produits InitialisationProduits(Produits produits) {
        return produitsR.save(produits);
    }

    @Override
    public Produits findByNomProduit(String nomP) {
        return findByNomProduit(nomP);
    }

    @Override
    public Produits findByPrix(double nbre) {
        return findByPrix(nbre);
    }

    @Override
    public Produits findByQuantite(double nbre) {
        return findByQuantite(nbre);
    }

    @Override
    public Produits findByDescription(String nomP) {
        return findByDescription(nomP);
    }




}
