package com.devtold.web.serviceImpl;

import com.devtold.web.Repository.AppRoleRepository;
import com.devtold.web.entites.securityEntiry.AppRole;
import com.devtold.web.service.AppRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AppRoleServiceImpl implements AppRoleService {
@Autowired
    private AppRoleRepository appRoleRepository;

    @Override
    public AppRole addNewRole(AppRole role) {
        return appRoleRepository.save(role);
    }

    @Override
    public List<AppRole> listRole() {
        return appRoleRepository.findAll();
    }

    @Override
    public AppRole findByRoleName(String name) {
        return appRoleRepository.findByRoleName(name);
    }
}
