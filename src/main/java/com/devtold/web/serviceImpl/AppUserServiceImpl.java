package com.devtold.web.serviceImpl;

import com.devtold.web.Repository.AppRoleRepository;
import com.devtold.web.Repository.AppUserRepository;
import com.devtold.web.entites.securityEntiry.AppRole;
import com.devtold.web.entites.securityEntiry.AppUser;
import com.devtold.web.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AppUserServiceImpl implements AppUserService {
    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private AppRoleRepository appRoleRepository ;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public AppUser addNewUser(AppUser userDto) {

        return appUserRepository.save(userDto);
    }

    @Override
    public AppUser loadUserByName(String name) {

        return appUserRepository.findByUserName(name);
    }

    @Override
    public List<AppUser> findAllUser() {
        return appUserRepository.findAll();
    }

    @Override
    public void addRoleToUser(String roleName, String userName) {
        AppUser appUser = appUserRepository.findByUserName(userName);
        AppRole addRole  = appRoleRepository.findByRoleName(roleName);
        appUser.getAppRoles().add(addRole);

    }

    @Override
    public AppUser findByUserName(String name) {
        return appUserRepository.findByUserName(name);
    }
}
