package com.devtold.web.serviceImpl;

import com.devtold.web.Repository.CategorieRepository;
import com.devtold.web.entites.Categories;
import com.devtold.web.service.CategorieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class CategorieServiceimpl implements CategorieService {

    @Autowired
    private CategorieRepository categorieRepository;

    @Override
    public List<Categories> listCategorie() {
        return categorieRepository.findAll();
    }

    @Override
    public Categories ajouterCategorie(Categories categories) {

        return categorieRepository.save(categories);
    }

    @Override
    public Categories modifierCategories(Categories categories) {
        categorieRepository.save(categories);
        return categories;
    }

    @Override
    public Categories supprimercategories(Long id) {
        categorieRepository.deleteById(id);

        return null;
    }

   @Override
    public Optional<Categories> findById(Long id) {
        return categorieRepository.findById(id);
    }


}
