package com.devtold.web.entites;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

@Entity
@Data
@Builder
public class Produits {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long produitId;
    @Column(name = "nom_produit",nullable = false)
    private String nomProduit;
    private String description;
    private double prix;
    private  double quantite;
    private boolean selectionne;
    private  String photo;
    @Transient
    private MultipartFile file;

   @ManyToOne
    @JoinColumn(name = "code_cat")
    private Categories categories;

    public Produits() {
   super();
    }

    public Produits(Long produitId, String nomProduit, String description,
                    double prix, double quantite, boolean selectionne, String photo,
                    MultipartFile file, Categories categories) {
        this.produitId = produitId;
        this.nomProduit = nomProduit;
        this.description = description;
        this.prix = prix;
        this.quantite = quantite;
        this.selectionne = selectionne;
        this.photo = photo;
        this.file = file;
        this.categories = categories;
    }
    public Produits(String libelle, String description, double quantites, Categories categories) {

    }

    //initialisation produit
    public  enum  produitsInit{
        ORDINATEUR("HP",CategoriesInit.technologique,"ordinateur de marque hp",100000,42),
        CAHIER("safca",CategoriesInit.education,"cahier safca afrique solide et bresistant",200,472),
        LIVRE("les champions en francais",CategoriesInit.education,"le champion en francais",1000,322),
        MEDICAMENTS("suprofloxacine",CategoriesInit.sante,"antibiotique ",1000,12);

        private  String libelle;
        private  String description;
        private  double prix;
        private double quantites;
        private CategoriesInit categoriesInit;

        produitsInit(String libelle,CategoriesInit categoriesInit,String description,double prix,double quantites){
            this.libelle=libelle;
            this.categoriesInit=categoriesInit;
            this.description=description;
            this.prix=prix;
            this.quantites=quantites;

        }
        public String getLibelle() {
            return libelle;
        }

        public CategoriesInit getCategoriesInit() {
            return categoriesInit;
        }

        public String getDescription() {
            return description;
        }

        public double getPrix() {
            return prix;
        }

        public double getQuantites() {
            return quantites;
        }
    }
    public enum CategoriesInit{
        education("education"),
        sante("sante"),
        beaute("beautes"),
        animal("animaux"),
        sportifs("sportifs"),
        enfants("enfants"),
        technologique("technologique"),
        artisanaux("artisanats"),
        alimentaire("alimentaire");

        private String lebelle;
        CategoriesInit(String lebelle){
            this.lebelle=getLebelle();
        }

        public String getLebelle() {
            return lebelle;
        }
    }


}
