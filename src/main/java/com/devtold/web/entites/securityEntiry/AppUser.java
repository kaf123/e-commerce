package com.devtold.web.entites.securityEntiry;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

/**
 * auteur: kana
 * date:09/01/23
 * but: gérer la sécurité de mon application web
 */

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AppUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;

    private  String userName;
    private  String passWord;
    @OneToMany
    Collection<AppRole> appRoles;


}
