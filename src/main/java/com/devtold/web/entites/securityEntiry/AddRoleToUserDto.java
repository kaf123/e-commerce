package com.devtold.web.entites.securityEntiry;

import lombok.Data;

@Data
public class AddRoleToUserDto {

  private  String username;
   private  String rolename;

}
