package com.devtold.web.entites;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * API pour télécharger une image
 * le but de cet excercice est de pouvoir ajouter et modifier une image
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class FileUploadProperties {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private  String url;

}
