package com.devtold.web.entites;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
public class Categories {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nomCategorie;
    private String description;
    private String photo;
    @Transient
    private MultipartFile file;

    @OneToMany(mappedBy="categories")
    Collection<Produits> produits;

    public Categories() {
        super();
    }

    public Categories(Long id, String nomCategorie, String description,
                      String photo, MultipartFile file, Collection<Produits> produits) {
        this.id = id;
        this.nomCategorie = nomCategorie;
        this.description = description;
        this.photo = photo;
        this.file = file;
        this.produits = produits;
    }
}
