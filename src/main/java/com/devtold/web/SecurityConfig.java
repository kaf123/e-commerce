package com.devtold.web;


import com.devtold.web.entites.securityEntiry.AppUser;
import com.devtold.web.filter.JwtAuthenticationFilter;
import com.devtold.web.serviceImpl.AppUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collection;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AppUserServiceImpl appUserService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //configuration des droits d acces a l'application
        auth.userDetailsService(new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                AppUser recuperationUserEnbd = appUserService.loadUserByName(username);
                Collection<GrantedAuthority> authorities = new ArrayList<>();
                recuperationUserEnbd.getAppRoles().forEach(r->{
                    authorities.add(new SimpleGrantedAuthority(r.getRoleName()));
                });
                return new User(recuperationUserEnbd.getUserName(), recuperationUserEnbd.getPassWord(),authorities);
            }
        });

        super.configure(auth);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //On specifie les droit d'acces
        //desctivation du csrf par ce qu'il utilise les session
        http.csrf().disable();
        // utilisation de l'auhentification stateless
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        // 1.autorisation d'access a toute les resources
        /**
         *         http.authorizeRequests().anyRequest().permitAll();
         *         explication: au lieu d utiliser permitAll pour donner acces a toutes les resources
         *         de lapplication, nous allons utilisees Authenticed(toutees les resources doivent
         *         necessite une authentification)
         */
        http.authorizeRequests().anyRequest().authenticated();
        //integrer le filtre (JwtauthenticationFilter) dans la configuration
        http.addFilter(new JwtAuthenticationFilter(authenticationManagerBean()));

    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}