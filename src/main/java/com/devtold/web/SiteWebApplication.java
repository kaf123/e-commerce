package com.devtold.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class SiteWebApplication implements CommandLineRunner {
	public static void main(String[] args) {
		SpringApplication.run(SiteWebApplication.class, args);
	}

	//Methode pour encoder le mot de passe
	//Le mot de pass est encoder chaque fois que l'on veut ajouter un utilisateur
	@Bean
	PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}
	@Override
	public void run(String... args) throws Exception {



	}






}
